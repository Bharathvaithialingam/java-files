
import org.openqa.selenium.*;

import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverExample2  {
    public static void main(String[] args) {
    	System.setProperty("webdriver.gecko.driver","C:\\Users\\bhara\\Desktop\\Testing\\Drivers\\geckodriver.exe");
    	
    	WebDriver driver = new FirefoxDriver(); //Opens FF browser
    	          
        driver.get("http://www.facebook.com"); // Enter facebook.com
        
        String tagName = driver.findElement(By.id("email")).getTagName(); // F E get tag
        
        System.out.println(tagName);
        
        driver.findElement(By.id("email")).sendKeys("Hello"); // Type hello in email text box
        
        driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("Hello");
        
       //driver.close();
        //System.exit(0);
}
}
